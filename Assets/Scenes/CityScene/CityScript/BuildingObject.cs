using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using TMPro;
using UnityEngine.Serialization;

namespace BuildingSystem
{
    public class BuildingObject : MonoBehaviour
    {
        public static BuildingObject current;

        public GridLayout gridlayout;
        private Grid _grid;
        [SerializeField] private Tilemap mainTilemap;
        [SerializeField] private TileBase whiteTile;
         
        public GameObject buildPanel;
        public GameObject UIbuildPanel;
        public TextMeshProUGUI ErrorText;
        public GameObject building;

        [FormerlySerializedAs("prefab1")] public GameObject prefabHouse;
        [FormerlySerializedAs("prefab2")] public GameObject prefabRice;
        [FormerlySerializedAs("prefab3")] public GameObject prefabSmith;
        [FormerlySerializedAs("prefab4")] public GameObject prefabJDSing;
        [FormerlySerializedAs("prefab5")] public GameObject prefabWood;
        [FormerlySerializedAs("prefab6")] public GameObject prefabJDluang;
        [FormerlySerializedAs("prefab7")] public GameObject prefabSamking;
        [FormerlySerializedAs("prefab8")] public GameObject prefabPanta;
        [FormerlySerializedAs("prefab9")] public GameObject prefabPark;
        [FormerlySerializedAs("prefab10")] public GameObject prefabChicken;

        private PlaceanableObject objectToPlace;
        #region Unity methods

        private void Awake()
        {
            current = this;
            _grid = gridlayout.gameObject.GetComponent<Grid>();
        }

        private void Update()
        {
            buildPanel = GameObject.Find("BuildPanel");
            if (!objectToPlace)
            {
                return;
            }
            if (Input.GetKeyDown(KeyCode.Space) && objectToPlace.Placed == false) 
            {
                Debug.Log("Wood"+objectToPlace.statWood);
                    Debug.Log("Rice"+objectToPlace.statRice);
                    Debug.Log("Iron"+objectToPlace.statIron);
                    Debug.Log("Money"+objectToPlace.statMoney);
                    Debug.Log("Gold"+objectToPlace.statGold);
                    Debug.Log("Faith"+objectToPlace.statFaith);
               
                if (CanBePlaced(objectToPlace))
                {
                    if (GameManager.wood >= objectToPlace.statWood &&
                        GameManager.rice >= objectToPlace.statRice &&
                        GameManager.iron >= objectToPlace.statIron &&
                        GameManager.money >= objectToPlace.statMoney &&
                        GameManager.gold >= objectToPlace.statGold &&
                        GameManager.faith >= objectToPlace.statFaith)
                        {
                            this.gameObject.GetComponent<AudioSource>().Play();
                            objectToPlace.Place();
                            UIbuildPanel.SetActive(false);
                            Vector3Int start = gridlayout.WorldToCell(objectToPlace.GetStartPosition());
                            TakeArea(start, objectToPlace.Size);
                        }else 
                        {
                            ErrorText.GetComponent<AudioSource>().Play();
                            ErrorText.SetText("Insufficient Resources");
                            StartCoroutine(TextCoroutine());
                            UIbuildPanel.SetActive(false);
                            Destroy(objectToPlace.gameObject);
                        } 
                }else
                {
                    ErrorText.GetComponent<AudioSource>().Play();
                    ErrorText.SetText("Not Enough Space");
                    StartCoroutine(TextCoroutine());
                  UIbuildPanel.SetActive(false);
                  Destroy(objectToPlace.gameObject);
                }
            }else if (Input.GetKeyDown(KeyCode.Escape) && objectToPlace.Placed == false)
            {
                UIbuildPanel.SetActive(false);
                Destroy(objectToPlace.gameObject);
            }
        }

        public void Build1()
        {
            
                InitializeWithObject(prefabHouse);
                buildPanel.SetActive(false);
                UIbuildPanel.SetActive(true);
                GameManager.isBuildPanelActive = false;
                

        }
        public void Build2()
        {
            
                InitializeWithObject(prefabRice);
                buildPanel.SetActive(false);
                UIbuildPanel.SetActive(true);
                GameManager.isBuildPanelActive = false;
            
        }
        public void Build3()
        {
           
                InitializeWithObject(prefabSmith);
                buildPanel.SetActive(false);
                UIbuildPanel.SetActive(true);
                GameManager.isBuildPanelActive = false;
            
        }
        public void Build4()
        {
            
                InitializeWithObject(prefabWood);
                buildPanel.SetActive(false);
                UIbuildPanel.SetActive(true);
                GameManager.isBuildPanelActive = false;
            
        }
        public void Build5()
        {
            if (GameManager.WatPraSingDone == false)
            {
                InitializeWithObject(prefabJDSing);
                buildPanel.SetActive(false);
                UIbuildPanel.SetActive(true);
                GameManager.isBuildPanelActive = false;
            }else if (GameManager.WatPraSingDone == true)
            {
                buildPanel.SetActive(false);
                ErrorText.SetText("You Already Build That");
                StartCoroutine(TextCoroutine());
            }
        }
        public void Build6()
        {
            if (GameManager.JDluangDone == false)
            {
                InitializeWithObject(prefabJDluang);
                buildPanel.SetActive(false);
                UIbuildPanel.SetActive(true);
                GameManager.isBuildPanelActive = false;
            }else if (GameManager.JDluangDone == true)
            {
                buildPanel.SetActive(false);
                ErrorText.SetText("You Already Build That");
                StartCoroutine(TextCoroutine());
            }
        }
        public void Build7()
        {
            if (GameManager.SamkingDone == false)
            {
                InitializeWithObject(prefabSamking);
                buildPanel.SetActive(false);
                UIbuildPanel.SetActive(true);
                GameManager.isBuildPanelActive = false;
            }else if (GameManager.SamkingDone == true)
            {
                buildPanel.SetActive(false);
                ErrorText.SetText("You Already Build That");
                StartCoroutine(TextCoroutine());
            }
        }
        public void Build8()
        {
            if (GameManager.WatpantaDone == false)
            {
                InitializeWithObject(prefabPanta);
                buildPanel.SetActive(false);
                UIbuildPanel.SetActive(true);
                GameManager.isBuildPanelActive = false;
            }else if (GameManager.WatpantaDone == true)
            {
                buildPanel.SetActive(false);
                ErrorText.SetText("You Already Build That");
                StartCoroutine(TextCoroutine());
            }
        }
        public void Build9()
        {
            if (GameManager.ParkDone == false)
            {
                InitializeWithObject(prefabPark);
                buildPanel.SetActive(false);
                UIbuildPanel.SetActive(true);
                GameManager.isBuildPanelActive = false;
            }else if (GameManager.ParkDone == true)
            {
                buildPanel.SetActive(false);
                ErrorText.SetText("You Already Build That");
                StartCoroutine(TextCoroutine());
            }
        }
        public void Build10()
        {
            if (GameManager.ChickenDone == false)
            {
                InitializeWithObject(prefabChicken);
                buildPanel.SetActive(false);
                UIbuildPanel.SetActive(true);
                GameManager.isBuildPanelActive = false;
            }else if (GameManager.ChickenDone == true)
            {
                buildPanel.SetActive(false);
                ErrorText.SetText("You Already Build That");
                StartCoroutine(TextCoroutine());
            }
        }
       
        #endregion

        #region Utils

        public static Vector3 GetMouseWorldPosition()
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out RaycastHit raycastHit))
            {
                return raycastHit.point;
            }
            else
            {
                return Vector3.zero;
            }
        }

        public Vector3 SnapCoordinateToGrid(Vector3 position)
        {
            Vector3Int cellPos = gridlayout.WorldToCell(position);
            position = _grid.GetCellCenterWorld(cellPos);
            return position;
        }

        private static TileBase[] GetTileBlock(BoundsInt area, Tilemap tilemap)
        {
            TileBase[] array = new TileBase[area.size.x * area.size.y * area.size.z];
            int counter = 0;

            foreach (var v in area.allPositionsWithin)
            {
                Vector3Int pos = new Vector3Int(v.x, v.y, 0);
                array[counter] = tilemap.GetTile(pos);
                counter++;
            }
            return array;
        }
        #endregion

        #region Placement

        public void InitializeWithObject(GameObject prefab)
        {
            Vector3 position = SnapCoordinateToGrid(Vector3.zero);
            GameObject obj = Instantiate(prefab, position, Quaternion.identity);
            objectToPlace = obj.GetComponent<PlaceanableObject>();
            obj.AddComponent<ObjectDrag>();
        }

        private bool CanBePlaced(PlaceanableObject placeanableObject)
        {
            BoundsInt area = new BoundsInt();
            area.position = gridlayout.WorldToCell(objectToPlace.GetStartPosition());
            area.size = placeanableObject.Size;

            TileBase[] baseArray = GetTileBlock(area, mainTilemap);
            foreach (var b in baseArray)
            {
                if (b == whiteTile)
                {
                    return false;
                }
            }

            return true;
        }

        public void TakeArea(Vector3Int start, Vector3Int size)
        {
            mainTilemap.BoxFill(start,whiteTile,start.x,start.y,start.x + size.x,start.y + size.y);
        }
        IEnumerator TextCoroutine()
        {
            yield return new WaitForSeconds(1.5f);
            ErrorText.SetText("");
        }
        #endregion
    }
}