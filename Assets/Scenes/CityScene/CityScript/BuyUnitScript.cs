using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyUnitScript : MonoBehaviour
{
    [Header("Melee")] 
    [SerializeField] private int mWood;
    [SerializeField] private int mRice;
    [SerializeField] private int mIron;
    [SerializeField] private int mMoney;
    [Header("Ranger")] 
    [SerializeField] private int rWood;
    [SerializeField] private int rRice;
    [SerializeField] private int rIron;
    [SerializeField] private int rMoney;
    [Header("Healer")] 
    [SerializeField] private int hWood;
    [SerializeField] private int hRice;
    [SerializeField] private int hIron;
    [SerializeField] private int hMoney;
    [Header("Tank")] 
    [SerializeField] private int tWood;
    [SerializeField] private int tRice;
    [SerializeField] private int tIron;
    [SerializeField] private int tMoney;
   
    public static int stmWood;
    public static int stmRice;
    public static int stmIron;
    public static int stmMoney;
    public static int strWood;
    public static int strRice;
    public static int strIron;
    public static int strMoney;
    public static int sthWood;
    public static int sthRice;
    public static int sthIron;
    public static int sthMoney;
    public static int sttWood;
    public static int sttRice;
    public static int sttIron;
    public static int sttMoney;

    private void Update()
    { 
         stmWood=mWood;
         stmRice=mRice;
         stmIron=mIron;
         stmMoney=mMoney;
         strWood=rWood;
         strRice=rRice;
         strIron=rIron;
         strMoney=rMoney;
         sthWood=hWood;
         sthRice=hRice;
         sthIron=hIron;
         sthMoney=hMoney;
         sttWood=tWood;
         sttRice=tRice;
         sttIron=tIron;
         sttMoney=tMoney;
    }

    public void BuyM()
    {
        if (GameManager.wood >= mWood && GameManager.rice >= mRice&&
            GameManager.iron >= mIron && GameManager.money >= mMoney )
        {
            GameManager.wood -= mWood;
            GameManager.rice -= mRice;
            GameManager.iron -= mIron;
            GameManager.money -= mMoney;
            GameManager.meleeNum += 1;
        }
        else
        {
            Debug.Log("NotEnoughResource");
        }
    }
    public void BuyR()
    {
        if (GameManager.wood >= rWood && GameManager.rice >= rRice&&
            GameManager.iron >= rIron && GameManager.money >= rMoney )
        {
            GameManager.wood -= rWood;
            GameManager.rice -= rRice;
            GameManager.iron -= rIron;
            GameManager.money -= rMoney;
            GameManager.rangerNum += 1;
        }
        else
        {
            Debug.Log("NotEnoughResource");
        }
    }
    public void BuyH()
    {
        if (GameManager.wood >= hWood && GameManager.rice >= hRice&&
            GameManager.iron >= hIron && GameManager.money >= hMoney )
        {
            GameManager.wood -= hWood;
            GameManager.rice -= hRice;
            GameManager.iron -= hIron;
            GameManager.money -= hMoney;
            GameManager.healerNum += 1;
        }
        else
        {
            Debug.Log("NotEnoughResource");
        }
    }
    public void BuyT()
    {
        if (GameManager.wood >= tWood && GameManager.rice >= tRice&&
            GameManager.iron >= tIron && GameManager.money >= tMoney )
        {
            GameManager.wood -= tWood;
            GameManager.rice -= tRice;
            GameManager.iron -= tIron;
            GameManager.money -= tMoney;
            GameManager.tankNum += 1;
        }
        else
        {
            Debug.Log("NotEnoughResource");
        }
    }
}
