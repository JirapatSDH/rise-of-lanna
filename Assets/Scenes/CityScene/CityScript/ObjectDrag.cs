using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BuildingSystem
{
    public class ObjectDrag : MonoBehaviour
    {
        private Vector3 offset;

        private void OnMouseDown()
        {
            offset = transform.position - BuildingObject.GetMouseWorldPosition();
        }
        private void Update()
        {
            Vector3 pos = BuildingObject.GetMouseWorldPosition() + offset;
            transform.position = BuildingObject.current.SnapCoordinateToGrid(pos);
        }
    }
}