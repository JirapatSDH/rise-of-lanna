using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public class GridDontDestroy : MonoBehaviour
{
    void Start()
    {
        for (int i = 0; i < Object.FindObjectsOfType<GridDontDestroy>().Length; i ++) 
        {
            if (Object.FindObjectsOfType<GridDontDestroy>()[i] != this)
            {
                if (Object.FindObjectsOfType<GridDontDestroy>()[i].name == gameObject.name)
                {
                    Destroy(gameObject);
                }
            }
        }
        DontDestroyOnLoad(gameObject);
    }
}