using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextScript : MonoBehaviour
{
    public TextMeshProUGUI TextUI;
    private string curDay;
    string dayEndText;
    string riceText;
    private string woodText;
    private string ironText;
    private string canExText;
    private string meText;
    private string raText;
    private string heText;
    private string taText;
    private string powText;
    
    void Update()
    {
        curDay = GameManager.dayCurrent.ToString();
        dayEndText = GameManager.dayEnd.ToString();
        riceText = GameManager.rice.ToString();
        woodText = GameManager.wood.ToString();
        ironText = GameManager.iron.ToString();
        canExText = GameManager.canExplore.ToString();
        meText = GameManager.meleeNum.ToString();
        raText = GameManager.rangerNum.ToString();
        heText = GameManager.healerNum.ToString();
        taText = GameManager.tankNum.ToString();
        powText = GameManager.manTotal.ToString();
        if (TextUI.name =="CurrentDay")
        {
            TextUI.text = "Day : "+curDay+"/"+dayEndText;
        }else if (TextUI.name == "Resource1")
        {
            TextUI.text = riceText;
        }else if (TextUI.name == "Resource2")
        {
            TextUI.text = woodText;
        }else if (TextUI.name == "Resource3")
        {
            TextUI.text = ironText;
        }
        else if (TextUI.name == "Resource4")
        {
            TextUI.text = GameManager.money.ToString();
        }
        else if (TextUI.name == "Resource5")
        {
            TextUI.text = GameManager.gold.ToString();
        }
        else if (TextUI.name == "Resource6")
        {
            TextUI.text = GameManager.faith.ToString();
        }
        else if (TextUI.name == "CanEx")
        {
            TextUI.text = canExText;
        }else if (TextUI.name == "MeleeNum")
        {
            TextUI.text = meText;
        }
        else if (TextUI.name == "RangerNum")
        {
            TextUI.text = raText;
        }else if (TextUI.name == "HealerNum")
        {
            TextUI.text = heText;
        }else if (TextUI.name == "TankNum")
        {
            TextUI.text = taText;
        }
        else if (TextUI.name == "Total")
        {
            TextUI.text = "Total power : "+powText;
        }
        else if (TextUI.name == "ObjectiveText")
        {
            TextUI.text = "Objective Done : " +GameManager.objectDone+ " / 6";
        }
    }
}
