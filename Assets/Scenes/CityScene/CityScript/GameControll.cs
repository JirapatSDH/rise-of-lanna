using System;
using System.Collections;
using System.Collections.Generic;
using BuildingSystem;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public class GameControll : MonoBehaviour
{
    [Header("Keys Config")]
     [SerializeField] protected Key add_rice = Key.J;
     [SerializeField] protected Key add_wood = Key.K;
     [SerializeField] protected Key add_iron = Key.L;
     public GameObject unitPanel;
     public GameObject buildPanel;
     public GameObject endPanel;
     public GameObject winimg;
     public GameObject Loseimg;
     public TextMeshProUGUI loseText;
     [Header("Cost")] 
     [SerializeField] public int meleeMaintainCost;
     [SerializeField] public int rangerMaintainCost;
     [SerializeField] public int healerMaintainCost;
     [SerializeField] public int tankMaintainCost;
     Animator fade_Animator;
     public GameObject RiceChange,WoodChange,IronChange,MoneyChange,
         RiceInstance,WoodInstance,IronInstance, MoneyInstance, Fade,
         SubRice,SubWood,SubIron,SubMoney;
     private int CurRice= 0,CurWood= 0,CurIron= 0,CurMoney = 0;
     void Update()
    {
        //build = GameObject.FindGameObjectWithTag("Grid").GetComponent<BuildingObject>();
        Keyboard keyboard = Keyboard.current;
        if (Input.GetKeyDown(KeyCode.X))
        {
            GameManager.wood += 1000;
            GameManager.rice += 1000;
            GameManager.iron += 1000;
            GameManager.money += 1000;
            GameManager.gold += 1000;
            GameManager.faith += 1000;
        }
        
        if (keyboard[add_rice].wasPressedThisFrame)
        {
            GameManager.rice++;
        }
        else if (keyboard[add_wood].wasPressedThisFrame)
        {
            GameManager.wood++;
        }
        else if (keyboard[add_iron].wasPressedThisFrame)
        {
            GameManager.iron++;
        }

        if (GameManager.dayCurrent == GameManager.dayEnd && GameManager.objectDone == 0)
        {
            MusicDonDestroy.instance.GetComponent<AudioSource>().Pause();
            endPanel.SetActive(true);
            Loseimg.SetActive(true);
            loseText.SetText("YOU LOSE");
        }else if (GameManager.objectDone == 6)
        {
            MusicDonDestroy.instance.GetComponent<AudioSource>().Pause();
            endPanel.SetActive(true);
            winimg.SetActive(true);
            loseText.SetText("YOU WIN");
            loseText.color= Color.green;
        }
        GameManager.manTotal = (GameManager.meleeNum * 1) + (GameManager.rangerNum * 2) + (GameManager.healerNum * 3) + (GameManager.tankNum * 4);
        if (GameManager.rice > CurRice)
        {
            Instantiate(RiceChange, RiceInstance.transform);
        }
        if (GameManager.rice < CurRice)
        {
            Instantiate(SubRice, RiceInstance.transform);
        }
        if (GameManager.rice != CurRice)
        {
            CurRice = GameManager.rice;
        }
        if (GameManager.wood > CurWood)
        {
            Instantiate(WoodChange, WoodInstance.transform);
        }
        if (GameManager.wood < CurWood)
        {
            Instantiate(SubWood, WoodInstance.transform);
        }
        if (GameManager.wood != CurWood)
        {
            CurWood = GameManager.wood;
        }
        if (GameManager.iron > CurIron)
        {
            Instantiate(IronChange, IronInstance.transform);
        }
        if (GameManager.iron < CurIron)
        {
            Instantiate(SubIron, IronInstance.transform);
        }
        if (GameManager.iron != CurIron)
        {
            CurIron = GameManager.iron;
        }
        if (GameManager.money > CurMoney)
        {
            Instantiate(MoneyChange, MoneyInstance.transform);
        }
        if (GameManager.money < CurMoney)
        {
            Instantiate(SubMoney, MoneyInstance.transform);
        }
        if (GameManager.money != CurMoney)
        {
            CurMoney = GameManager.money;
        }
    }

    public void EndDay()
    {
        Fade = GameObject.FindGameObjectWithTag("Fade");
        fade_Animator = Fade.GetComponent<Animator>();
        GameManager.dayCurrent++;
        GameManager.canExplore = true;
        if(GameManager.NextDay == false)
        {
            GameManager.NextDay = true;
        }
        MaintainCost();
        fade_Animator.SetTrigger("Fade");
        StartCoroutine(DayCoroutine());
    }
    public void Explore()
    {
        if ( GameManager.canExplore == true)
        {
            SceneManager.LoadScene(2);
            GameManager.canExplore = false;
        }
    }
    public void OpenUnitList()
    {
        unitPanel.SetActive(true);
    }
    public void CloseUnitList()
    {
        unitPanel.SetActive(false);
    }
    public void OpenBuildList()
    {
        buildPanel.SetActive(true);
        GameManager.isBuildPanelActive = true;
    }
    public void CloseBuildList()
    {
        buildPanel.SetActive(false);
        GameManager.isBuildPanelActive = false;
    }
    IEnumerator DayCoroutine()
    {
        yield return new WaitForSeconds(0.2f);
        GameManager.NextDay = false;
    }

    void MaintainCost()
    {
        if (GameManager.meleeNum >= 1)
        {
            GameManager.rice -= meleeMaintainCost;
        }
        if (GameManager.rangerNum >= 1)
        {
            GameManager.rice -= rangerMaintainCost;
        }
        if (GameManager.healerNum >= 1)
        {
            GameManager.rice -= healerMaintainCost;
        }
        if (GameManager.tankNum >= 1)
        {
            GameManager.rice -= tankMaintainCost;
        }
    }
}
