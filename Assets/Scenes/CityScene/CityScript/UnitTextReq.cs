using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UnitTextReq : MonoBehaviour
{
    public TextMeshProUGUI TextUI;
    private int mWood;
    private int mRice;
    private int mIron;
    private int mMoney;
    
    private int rWood;
    private int rRice;
    private int rIron;
    private int rMoney;
    
    private int hWood;
    private int hRice;
    private int hIron;
    private int hMoney;
    
    private int tWood;
    private int tRice;
    private int tIron;
    private int tMoney;
    void Update()
    {
        mWood = BuyUnitScript.stmWood;
        mRice = BuyUnitScript.stmRice;
        mIron = BuyUnitScript.stmIron;
        mMoney= BuyUnitScript.stmMoney;
        rWood = BuyUnitScript.strWood;
        rRice=BuyUnitScript.strRice;
        rIron=BuyUnitScript.strIron;
        rMoney=BuyUnitScript.strMoney;
        hWood=BuyUnitScript.sthWood;
        hRice=BuyUnitScript.sthRice;
        hIron=BuyUnitScript.sthIron;
        hMoney=BuyUnitScript.sthMoney;
        tWood=BuyUnitScript.sttWood;
        tRice=BuyUnitScript.sttRice;
        tIron=BuyUnitScript.sttIron;
        tMoney=BuyUnitScript.sttMoney;
        
        if (TextUI.name =="MeleeReq")
        {
            TextUI.text = "Rice :"+mRice+ " Wood :"+mWood+ " Iron :"+mIron+" Money :"+mMoney;
        }else if (TextUI.name == "RangerReq")
        {
            TextUI.text = TextUI.text = "Rice :"+rRice+ " Wood :"+rWood+ " Iron :"+rIron+" Money :"+rMoney; ;
        }else if (TextUI.name == "HealerReq")
        {
            TextUI.text = TextUI.text = "Rice :"+hRice+ " Wood :"+hWood+ " Iron :"+hIron+" Money :"+hMoney; ;
        }else if (TextUI.name == "TankReq")
        {
            TextUI.text = TextUI.text = "Rice :"+tRice+ " Wood :"+tWood+ " Iron :"+tIron+" Money :"+tMoney; ;
        } 
    }
}
