using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BuildingSystem
{
    public class PlaceanableObject : MonoBehaviour
    {
        public bool Placed { get; private set; }
        public  int statWood { get; private set; }
        public  int statRice{ get; private set; }
        public  int statIron{ get; private set; }
        public  int statMoney{ get; private set; }
        public  int statGold{ get; private set; }
        public  int statFaith{ get; private set; }
        public Vector3Int Size { get; private set; }
        [SerializeField] public int reqWood;
        [SerializeField] public int reqRice;
        [SerializeField] public int reqIron;
        [SerializeField] public int reqMoney;
        [SerializeField] public int reqGold;
        [SerializeField] public int reqFainth;
        [SerializeField] public int Turn;
        
        private Vector3[] Vertices;
        private string obj;
        private bool defColleted = false;
        private bool isCollected;
        private int lastDay;
        private int defTurn;
        private GameObject objWithResource;
        private void GetColliderVertexPositionsLocal()
        {
            BoxCollider b = gameObject.GetComponent<BoxCollider>();
            Vertices = new Vector3[4];
            Vertices[0] = b.center + new Vector3(-b.size.x, -b.size.y, -b.size.z) * 0.5f;
            Vertices[1] = b.center + new Vector3(b.size.x, -b.size.y, -b.size.z) * 0.5f;
            Vertices[2] = b.center + new Vector3(b.size.x, -b.size.y, b.size.z) * 0.5f;
            Vertices[3] = b.center + new Vector3(-b.size.x, -b.size.y, b.size.z) * 0.5f;
        }

        private void CalculateSizeInCells()
        {
            Vector3Int[] vertices = new Vector3Int[Vertices.Length];
            for (int i = 0; i < vertices.Length; i++)
            {
                Vector3 worldPos = transform.TransformPoint(Vertices[i]);
                vertices[i] = BuildingObject.current.gridlayout.WorldToCell(worldPos);
            }

            Size = new Vector3Int(Math.Abs((vertices[0] - vertices[1]).x), Math.Abs((vertices[0] - vertices[3]).y), 1);
        }

        public Vector3 GetStartPosition()
        {
            return transform.TransformPoint(Vertices[0]);
        }

        private void Awake()
        {
            if (this.gameObject.name == "pivotHouse(Clone)")
            {
                GameObject originalGameObject = gameObject;
                objWithResource = originalGameObject.transform.GetChild(1).gameObject;
                Debug.Log(objWithResource);
            }else if (this.gameObject.name == "pivotRice(Clone)")
            {
                GameObject originalGameObject = gameObject;
                objWithResource = originalGameObject.transform.GetChild(0).gameObject;
                Debug.Log(objWithResource);
            }else if (this.gameObject.name == "pivotSmith(Clone)")
            {
                GameObject originalGameObject = gameObject;
                objWithResource = originalGameObject.transform.GetChild(1).gameObject;
                Debug.Log(objWithResource);
            }else if (this.gameObject.name == "pivotWood(Clone)")
            {
                GameObject originalGameObject = gameObject;
                objWithResource = originalGameObject.transform.GetChild(0).gameObject;
                Debug.Log(objWithResource);
            }
        }

        private void Start()
        {
            GetColliderVertexPositionsLocal();
            CalculateSizeInCells();
            defTurn = Turn;
            lastDay = GameManager.dayCurrent;
            obj = this.gameObject.name;
            isCollected = true;
        }
        private void Update()
        {
            /*if (GameManager.NextDay == true && Placed == true)
            {
                Reset();
            }*/
            if (lastDay != GameManager.dayCurrent && Placed == true)
            {
                Turn--;
                Reset();
                lastDay = GameManager.dayCurrent;
            }
            if (Turn <= 0)
            {
                Turn = 0;
            }
            if (this.gameObject.name == "pivotHouse(Clone)" && Turn <= 0 && Placed == true)
            {
                objWithResource.SetActive(true);
            }
            if (this.gameObject.name == "pivotHouse(Clone)" && Turn > 0 && Placed == true)
            {
                objWithResource.SetActive(false);
            }
            if (this.gameObject.name == "pivotRice(Clone)" && Turn <= 0 && Placed == true)
            {
                objWithResource.SetActive(true);
            }
            if (this.gameObject.name == "pivotRice(Clone)" && Turn > 0 && Placed == true)
            {
                objWithResource.SetActive(false);
            }
            if (this.gameObject.name == "pivotSmith(Clone)" && Turn <= 0 && Placed == true)
            {
                objWithResource.SetActive(true);
            }
            if (this.gameObject.name == "pivotSmith(Clone)" && Turn > 0 && Placed == true)
            {
                objWithResource.SetActive(false);
            }
            if (this.gameObject.name == "pivotWood(Clone)" && Turn <= 0 && Placed == true)
            {
                objWithResource.SetActive(true);
            }
            if (this.gameObject.name == "pivotWood(Clone)" && Turn > 0 && Placed == true)
            {
                objWithResource.SetActive(false);
            }
            if (GameManager.isBuildPanelActive == true && Placed == false)
            {
                Destroy(gameObject);
            }
            statWood = reqWood;
            statRice = reqRice;
            statIron = reqIron;
            statMoney = reqMoney;
            statGold = reqGold;
            statFaith = reqFainth;
        }  
        public virtual void Place()
        {
            ObjectDrag drag = gameObject.GetComponent<ObjectDrag>();
            Destroy(drag);
            Placed = true;
            if (GameManager.wood >= reqWood &&
                GameManager.rice >= reqRice &&
                GameManager.iron >= reqIron &&
                GameManager.money >= reqMoney &&
                GameManager.gold >= reqGold &&
                GameManager.faith >= reqFainth)
            {
                
                GameManager.wood -= reqWood;
                GameManager.rice -= reqRice;
                GameManager.iron -= reqIron;
                GameManager.money -= reqMoney;
                GameManager.gold -= reqGold;
                GameManager.faith -= reqFainth;
            }
            if (obj == "pivotJD(Clone)")
                 {
                     GameManager.WatPraSingDone = true;
                     GameManager.objectDone++;
                 }else if (obj == "pivotJDLuang(Clone)")
                 {
                     GameManager.JDluangDone = true;
                     GameManager.objectDone++;
                 }else if (obj == "pivotSamKing(Clone)")
                 {
                     GameManager.SamkingDone = true;
                     GameManager.objectDone++;
                 }else if (obj == "pivotPanta(Clone)")
                 {
                     GameManager.WatpantaDone = true;
                     GameManager.objectDone++;
                 }else if (obj == "pivotPark(Clone)")
                 {
                     GameManager.ParkDone = true;
                     GameManager.objectDone++;
                 }else if (obj == "pivotChicken(Clone)")
                 {
                     GameManager.ChickenDone = true;
                     GameManager.objectDone++;
                 }
        }
        private void OnMouseDown()
        {
            if (isCollected == false && Placed == true && Turn == 0)
            {
                isCollected = true;
                Turn = defTurn;
                AddResourse();
            }
        }
        public void AddResourse()
        {
            if (obj == "pivotHouse(Clone)")
            {
                GameManager.money += 5;
            }else if (obj == "pivotRice(Clone)")
            {
                GameManager.rice += 3;
            }else if (obj == "pivotSmith(Clone)")
            {
                GameManager.iron += 4;
            }else if (obj == "pivotWood(Clone)")
            {
                GameManager.wood += 4;
            }else if (obj == "pivotJD(Clone)")
            {
                GameManager.faith += 1;
            }else if (obj == "pivotJDLuang(Clone)")
            {
                GameManager.faith += 1;
            }else if (obj == "pivotSamKing(Clone)")
            {
                GameManager.gold += 1;
            }else if (obj == "pivotPanta(Clone)")
            {
                GameManager.faith += 1;
            }else if (obj == "pivotPark(Clone)")
            {
                GameManager.wood += 4;
                GameManager.money += 50;
            }else if (obj == "pivotChicken(Clone)")
            {
                GameManager.rice += 3;
                GameManager.money += 50;
            }
        }
        
        private void Reset()
        {
            isCollected = defColleted;
        }
    }
}