using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public EventScript EventList;
    [SerializeField] public int meleeMaintainCost;
    [SerializeField] public int rangerMaintainCost;
    [SerializeField] public int healerMaintainCost;
    [SerializeField] public int tankMaintainCost;
    [SerializeField] public  int WoodPerTurn;
    [SerializeField] public  int RicePerTurn;
    [SerializeField] public  int IronPerTurn;
    [SerializeField] public  int MoneyPerTurn;
    [SerializeField] public  int GoldPerTurn;
    [SerializeField] public  int FaithPerTurn;
    //difficult
    
    //day
    public static int dayCurrent = 0;
    public static int dayEnd = 30;

    //resourse
    public static int rice = 10;
    public static int wood = 8;
    public static int iron = 8;
    public static int money = 7;
    public static int gold = 0;
    public static int faith = 0;

    public static bool canExplore = true;
    public static bool NextDay = false;
    //unit
    public static int meleeNum= 0;
    public static int rangerNum= 0;
    public static int healerNum= 0;
    public static int tankNum= 0;
    public static int manTotal;
    
    public static int statmeleeMaintainCost;
    public static int statrangerMaintainCost;
    public static int stathealerMaintainCost;
    public static int stattankMaintainCost;

    //object
    public static int objectDone = 0;
    
    public static bool WatPraSingDone = false;
    public static bool JDluangDone = false;
    public static bool SamkingDone = false;
    public static bool WatpantaDone = false;
    public static bool ParkDone = false;
    public static bool ChickenDone = false;
    
    //Add resousce
    
    public static int AddWood;
    public static int AddRice;
    public static int AddIron;
    public static int AddMoney;
    public static int AddGold;
    public static int AddFaith;
    public static bool isBuildPanelActive;
    void Awake()
    {
        Instance = this;
    }
    private void Update()
    {
    }
}
