using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicDonDestroy : MonoBehaviour
{
   public static MusicDonDestroy instance;
   private void Awake()
   {
      GameObject[] obj = GameObject.FindGameObjectsWithTag("Music");
      if (obj.Length>1 || instance!=null)
      {
         Destroy(this.gameObject);
      }
      else
      {
         instance = this;
         DontDestroyOnLoad(this.gameObject);
      }
   }
}
