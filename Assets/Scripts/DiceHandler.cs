using UnityEngine;

public class DiceHandler : MonoBehaviour
{
    private bool hasRolled = false;
    private Rigidbody rb;
    public ObjectLabeller TheLabeller;
    public int TheThing;
    private bool ishitted = false;
    private BoxCollider boxCollider; // Add this variable
    private MeshRenderer therenderer;
    public InBattleSceneCollision Coliderthingy;
    public GameStatus thegamestats;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        boxCollider = GetComponent<BoxCollider>();
        therenderer = GetComponent<MeshRenderer>();
        TheLabeller = GetComponent<ObjectLabeller>();
    }
    public void performroll() {
            therenderer.enabled = true;
            TheLabeller.showtext();
            // send roll status message
             // Apply a random force to the dice to make it roll in the air
            float fixedYForce = 5f; // Fixed force value on the Y-axis
            float randomXForce = Random.Range(-5f, 5f); // Random force value on the X-axis between -5 and 5
            float randomZForce = Random.Range(-5f, 5f); // Random force value on the Z-axis between -5 and 5
            Vector3 randomForce = new Vector3(randomXForce, fixedYForce, randomZForce);
            rb.AddForce(randomForce, ForceMode.Impulse);

            // Apply a random torque to give the dice a spin
            Vector3 randomTorque = Random.onUnitSphere * 40f;
            rb.AddTorque(randomTorque, ForceMode.Impulse);

            // Mark the dice as rolled to prevent applying force again
            hasRolled = true;
    }
    void Update()
    {
        // Check if the dice has not rolled yet and the left mouse button is clicked
        if (rb.IsSleeping() && Input.GetKeyDown("e") && Coliderthingy.isplayercollided == true)
        {
            thegamestats.StartRoll();
        }

        // Check if the dice has rolled and has come to a stop
        if (hasRolled && rb.IsSleeping() && (!ishitted))
        {
            // Perform the raycast
            PerformRaycast();
        }
    }

    void PerformRaycast()
    {
        
        Vector3 startPosition = transform.position + new Vector3(0,2,0);

        Vector3 direction = (transform.position) - startPosition ;

        Ray ray = new Ray(startPosition, direction);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject.name.Length == 7 && hit.collider.gameObject.tag == "Dice") {
                ishitted = true;
                TheThing = System.Convert.ToInt32(hit.collider.gameObject.name.Substring( 6, 1));;
                thegamestats.addscore(TheThing);
            } else {
                performroll();
            }
        }
        
    }
}

