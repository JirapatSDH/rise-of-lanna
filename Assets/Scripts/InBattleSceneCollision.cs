using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InBattleSceneCollision : MonoBehaviour
{

    public bool isplayercollided = false;
    public Image Thingy;

    // Gets called at the start of the collision 
    void OnTriggerEnter(Collider collision)
    {
        Thingy.enabled = true;
        isplayercollided = true;
        Debug.Log("Entered collision with " + collision.gameObject.name);
    }


    // Gets called when the object exits the collision
    void OnTriggerExit(Collider collision)
    {
        Thingy.enabled = false;
        isplayercollided = false;
        Debug.Log("Exited collision with " + collision.gameObject.name);
    }

    void Awake() {
        Thingy.enabled = false;

    }
}
