using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using StarterAssets;

public class GameStatus : MonoBehaviour
{


    private string[] ResourcesToGive = new string[2]; // A table with 5 elements in this example

    public GameObject CinemachineCameraTarget;
    private int maxresource = 8;
    private int PlayerTotalScore = 0;
    private int UnitPowerUp = 0;

    private int rolleddice = 0;
    private float EnemyDice = 0;

    private bool CanRenderTopDownCam = false;

    [SerializeField] public TextMeshProUGUI EnemyText;    // Start is called before the first frame update
    [SerializeField] public TextMeshProUGUI PlayerText;    // Start is called before the first frame update

    [SerializeField] public TextMeshProUGUI ConditionText;    // Start is called before the first frame update
    [SerializeField] public TextMeshProUGUI DisplayText;    // Start is called before the first frame update

    [SerializeField] public TextMeshProUGUI GainText;    // Start is called before the first frame update
    [SerializeField] public TextMeshProUGUI YouGainText;    // Start is called before the first frame update

    [SerializeField] public GameObject PlayerChar;    // Start is called before the first frame update


    [SerializeField] public TextMeshProUGUI CanvasEnemyText;    // Start is called before the first frame update
    [SerializeField] public TextMeshProUGUI CanvasPlayerText;

    [SerializeField] public Image CanvasBGEnemy;    // Start is called before the first frame update
    [SerializeField] public Image CanvasBGPlayer;
    [SerializeField] public StarterAssetsInputs CharacterInput;

    [SerializeField] public List<GameObject> WalkToObjects;
    [SerializeField] public List<WalkToScript> TheUnit;
    [SerializeField] public List<WalkToScript> TheEnemy;

    [SerializeField] public List<DiceHandler> TheDiceHandler;

    [SerializeField] public ParticleSystem thepart;

    private void performdecreaseunit()
    {
        if (UnitPowerUp <= 0)
        {
            GainText.text = "Nothing";
        }
        else
        {
            switch (Random.Range(1, 5))
            {
                case 1:
                    if (GameManager.meleeNum > 0)
                    {
                        GameManager.meleeNum = Mathf.Max(GameManager.meleeNum - 1, 0);
                        GainText.text = "1 melee unit";
                        break;
                    }
                    else
                    {
                        performdecreaseunit();
                        break;
                    }
                case 2:

                    if (GameManager.rangerNum > 0)
                    {
                        GameManager.rangerNum = Mathf.Max(GameManager.rangerNum - 1, 0);
                        GainText.text = "1 ranger unit";
                        break;
                    }
                    else
                    {
                        performdecreaseunit();
                        break;
                    }
                case 3:
                    if (GameManager.healerNum > 0)
                    {
                        GameManager.healerNum = Mathf.Max(GameManager.healerNum - 1, 0);
                        GainText.text = "1 healer unit";
                        break;
                    }
                    else
                    {
                        performdecreaseunit();
                        break;
                    }
                case 4:
                    if (GameManager.tankNum > 0)
                    {
                        GameManager.tankNum = Mathf.Max(GameManager.tankNum - 1, 0);
                        GainText.text = "1 tank unit";
                        break;
                    }
                    else
                    {
                        performdecreaseunit();
                        break;
                    }
                default:
                    GainText.text = "Nothing";
                    break; 
            }

        }

    }
    
    private void checkcondition()
    {
        if (PlayerTotalScore >= EnemyDice)
        {
            ConditionText.text = "You win this round";

            int currentresource = maxresource;

            int RandomRiceGain = Mathf.Min(Random.Range(1, currentresource), 4);
            currentresource -= RandomRiceGain;

            int RandomRiceGain2 = Mathf.Min(Random.Range(1, currentresource), 4);
            currentresource -= RandomRiceGain2;

            int RandomRiceGain3 = currentresource;

            Debug.Log("Rice Gain: " + RandomRiceGain);
            Debug.Log("Wood Gain: " + RandomRiceGain2);
            Debug.Log("Iron Gain: " + RandomRiceGain3);

            GainText.text = RandomRiceGain + " Rice | " + RandomRiceGain2 + " Wood | " + RandomRiceGain3 + " Iron";
            GainText.enabled = true;
            YouGainText.enabled = true;
            GameManager.rice += RandomRiceGain;
            GameManager.wood += RandomRiceGain2;
            GameManager.iron += RandomRiceGain3;

            GameManager.gold += 5;
            GameManager.faith += 5;

        }
        else
        {
            ConditionText.text = "You lose this round";
            YouGainText.text = "you lose";
            YouGainText.enabled = true;
            // Lose One of unit
            performdecreaseunit();
        }
        GainText.enabled = true;

        ConditionText.enabled = true;
        thepart.Pause();

        StartCoroutine(SwitchSceneCoroutine(1, 2f));
    }
    void Start()
    {
        CharacterInput.SetCursorState(true);

        int meleeNum = GameManager.meleeNum;
        int rangerNum = GameManager.rangerNum;
        int healerNum = GameManager.healerNum;
        int tankNum = GameManager.tankNum;
        UnitPowerUp = (meleeNum * 1) + (rangerNum * 2) + (healerNum * 3) + (tankNum * 4);
        PlayerTotalScore += (meleeNum * 1) + (rangerNum * 2) + (healerNum * 3) + (tankNum * 4);


        // Enemy Day Calculations
        float Passive = Mathf.Max(0, (GameManager.dayCurrent));

        if (Passive > 0)
        {
            Debug.Log("Enemy Passive Score: " + Passive);
        }

        // random enemy score
        EnemyDice = Random.Range(2, 12) + Passive;
        EnemyText.text = "" + EnemyDice;
        PlayerText.text = "" + PlayerTotalScore;

        CanvasEnemyText.text = "Enemy Score: " + EnemyDice;
        CanvasPlayerText.text = "Your Score: " + PlayerTotalScore;

    }

    public void StartRoll()
    {
        //DisplayText.enabled = false;
        CinemachineCameraTarget.SetActive(false);
        PlayerChar.SetActive(false);
        CanRenderTopDownCam = true;
        CanvasEnemyText.enabled = true;
        CanvasPlayerText.enabled = true;
        CanvasBGEnemy.enabled = true;
        CanvasBGPlayer.enabled = true;
        ConditionText.GetComponent<AudioSource>().Play();
        foreach (var item in TheUnit)
        {
            // getting random position
            var element = WalkToObjects[Random. Range(0, WalkToObjects.Count)];

            item.performwalk(element.transform.position);
        }

        foreach (var item in TheEnemy)
        {
            // getting random position
            var element = WalkToObjects[Random. Range(0, WalkToObjects.Count)];

            item.performwalk(element.transform.position);
        }
        StartCoroutine(EnableSmoke());
        StartCoroutine(AddDelayToRoll());

    }


    public void addscore(int score)
    {
        if (rolleddice < 2)
        {
            rolleddice++;
            PlayerTotalScore += score;
            PlayerText.text = "" + PlayerTotalScore;
            CanvasPlayerText.text = "Your Score: " + PlayerTotalScore;

            if (rolleddice == 2)
            {
                checkcondition();
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (CanRenderTopDownCam)
        {

            // start rendering
            Vector3 newposition = new Vector3(545, 20, -5);
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, newposition, 0.1f);
            Camera.main.transform.rotation = Quaternion.Euler(new Vector3(90, 0, 0));

        }
    }

    private IEnumerator SwitchSceneCoroutine(int sceneName, float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);
        CharacterInput.SetCursorState(false);
        SceneManager.LoadScene(sceneName);
    }

    private IEnumerator EnableSmoke()
    {

        yield return new WaitForSeconds(1.5f);
        thepart.Play();
    }

    private IEnumerator AddDelayToRoll()
    {

        yield return new WaitForSeconds(3f);

        foreach (var item in TheDiceHandler)
        {
            item.performroll();
        }
    }
}
