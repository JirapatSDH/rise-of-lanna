using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkToScript : MonoBehaviour
{

    private Vector3 DesitnationPosition;
    private Vector3 StartPosition;
    private bool canwalk = false;

    private float startTime;

    // Total distance between the markers.
    private float journeyLength;

    public void performwalk(Vector3 NewVector) {
        DesitnationPosition = NewVector;
        StartPosition = this.transform.position;
        canwalk = true;
        startTime = Time.time;

        // Calculate the journey length.
        journeyLength = Vector3.Distance(StartPosition, DesitnationPosition);
        transform.LookAt(DesitnationPosition);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (canwalk) {
            
            float distCovered = (Time.time - startTime) * 5F;

            // Fraction of journey completed equals current distance divided by total distance.
            float fractionOfJourney = distCovered / journeyLength;


            transform.position = Vector3.Lerp(StartPosition, DesitnationPosition, fractionOfJourney);
        }
    }
}
