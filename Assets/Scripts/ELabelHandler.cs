using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ELabelHandler : MonoBehaviour
{

    private Canvas m_Canvas;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_Canvas.transform.rotation = Quaternion.LookRotation( m_Canvas.transform.position - Camera.main.transform.position );
    }

    private void Awake()
        {
            
            m_Canvas = this.GetComponent<Canvas>();
        }
}
