using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectTu : MonoBehaviour
{
    public GameObject Tu1, Tu2, Tu3, Tu4;
    private int page = 0;

    private void Update()
    {
        if (page == 0)
        {
            Tu1.SetActive(true);
            Tu2.SetActive(false);
            Tu3.SetActive(false);
            Tu4.SetActive(false);
        }
        if (page == 1)
        {
            Tu1.SetActive(false);
            Tu2.SetActive(true);
            Tu3.SetActive(false);
            Tu4.SetActive(false);
        }
        if (page == 2)
        {
            Tu1.SetActive(false);
            Tu2.SetActive(false);
            Tu3.SetActive(true);
            Tu4.SetActive(false);
        }
        if (page == 3)
        {
            Tu1.SetActive(false);
            Tu2.SetActive(false);
            Tu3.SetActive(false);
            Tu4.SetActive(true);
        }
    }

    public void NextPage()
    {
        page += 1;
        if (page >= 3)
        {
            page = 3;
        }
    }
    public void PrePage()
    {
        page -= 1;
        if (page <= 0)
        {
            page = 0;
        }
    }
}
